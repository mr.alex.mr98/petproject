package general.service.requests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.sql.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import general.service.utils.converter.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class CategoryServiceTest {
	
	@Autowired
	CategoryService categoryService;
	
	@MockBean
	CategoryRepository categoryRepository;
	
	@MockBean
	ReportObjToDTO reportConverter;
	
	@MockBean
	CategoryEntityToDTO converter;
	
	@MockBean
	UserRepository userRepository;
	
	@Test
	public void findAll() {
		CategoryEntity category = new CategoryEntity(new UserEntity("1", "1", "name"), "cat");
		List<CategoryEntity> categories = Collections.singletonList(category);
		CategoryDTO categoryDTO = new CategoryDTO(1, "catDTO");
		when(converter.convert(category)).thenReturn(categoryDTO);
		when(categoryRepository.findAllByUserId(1)).thenReturn(categories);
		
		List<CategoryDTO> cats = categoryService.findAll(1);
		
		assertTrue(cats.size() == 1);
		assertTrue(cats.get(0)
					   .getName()
					   .equals("catDTO"));
		assertTrue(cats.get(0)
					   .getId() == 1);
	}
	
	@Test
	public void create_created() {
		UserEntity user = new UserEntity("1", "1", "name");
		CategoryEntity categoryDAO = new CategoryEntity(user, "name");
		
		when(categoryRepository.findByNameAndUserId("name", 1)).thenReturn(Optional.empty());
		when(userRepository.getById(1)).thenReturn(user);
		when(categoryRepository.save(any(CategoryEntity.class))).thenReturn(categoryDAO);
		
		categoryService.create("name", 1);
		
		verify(categoryRepository, times(1)).save(any(CategoryEntity.class));
	}
	
	@Test
	public void create_notCreated() {
		UserEntity user = new UserEntity("1", "1", "name");
		CategoryEntity categoryDAO = new CategoryEntity(user, "name");
		
		when(categoryRepository.findByNameAndUserId("name", 1)).thenReturn(Optional.of(categoryDAO));
		when(userRepository.getById(1)).thenReturn(user);
		when(categoryRepository.save(any(CategoryEntity.class))).thenReturn(categoryDAO);
		
		categoryService.create("name", 1);
		
		verify(categoryRepository, times(0)).save(any(CategoryEntity.class));
	}
	
	@Test
	public void delete_true() {
		CategoryEntity categoryDAO = new CategoryEntity(new UserEntity("1", "1", "name"), "cat");
		when(categoryRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(categoryDAO));
		
		assertTrue(categoryService.delete(1, 1));
	}
	
	@Test
	public void delete_false() {
		when(categoryRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.empty());
		
		assertFalse(categoryService.delete(1, 1));
	}
	
	@Test
	public void change_true() {
		when(categoryRepository.update(1, "name", 1)).thenReturn(1);
		
		assertTrue(categoryService.change(1, "name", 1));
	}
	
	@Test
	public void change_false() {
		when(categoryRepository.update(1, "name", 1)).thenReturn(0);
		
		assertFalse(categoryService.change(1, "name", 1));
	}
	
	@Test
	public void report() {
		Object[] reportDAO = {"cat", 1};
		List<Object[]> reportList = new ArrayList<>();
		reportList.add(reportDAO);
		ReportDTO reportDTO = new ReportDTO("cat", 1);
		when(reportConverter.convert(reportDAO)).thenReturn(reportDTO);
		when(categoryRepository.reportOnPeriod(Timestamp.valueOf("2022-01-01 00:00:01"),
											   Timestamp.valueOf("2022-01-02 00:00:01"),
											   1))
			.thenReturn(reportList);
		
		List<ReportDTO> report = categoryService.report(Timestamp.valueOf("2022-01-01 00:00:01"),
														Timestamp.valueOf("2022-01-02 00:00:01"),
														1);
		
		assertEquals(1, report.size());
		assertEquals("cat", report.get(0)
								  .getCatName());
		assertEquals("1", report.get(0)
								.getValue()
								.toString());
	}
}