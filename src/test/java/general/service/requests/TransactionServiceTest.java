package general.service.requests;

import static org.mockito.Mockito.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.math.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class TransactionServiceTest {
	
	@Autowired
	TransactionService transactionService;
	
	@MockBean
	TransactionRepository transactionRepository;
	
	@MockBean
	AccountRepository accountRepository;
	
	@MockBean
	CategoryRepository categoryRepository;
	
	@Test
	public void make_made() {
		UserEntity user = new UserEntity("email", "password", "name");
		AccountEntity fromAcc = new AccountEntity(user, "fromAcc");
		fromAcc.setBalance(BigDecimal.valueOf(1000));
		AccountEntity toAcc = new AccountEntity(user, "toAcc");
		toAcc.setBalance(BigDecimal.valueOf(0));
		CategoryEntity category = new CategoryEntity(user, "cat");
		
		TransactionEntity transaction = new TransactionEntity(BigDecimal.valueOf(1000), new Date(100), fromAcc, toAcc, Collections.singletonList(category));
		
		when(accountRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(fromAcc));
		when(accountRepository.findByIdAndUserId(2, 1)).thenReturn(Optional.of(toAcc));
		when(categoryRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(category));
		when(transactionRepository.save(any(TransactionEntity.class))).thenReturn(transaction);
		
		transactionService.make(1, 2, BigDecimal.valueOf(1000), 1, 1);
		
		verify(transactionRepository, times(1)).save(any(TransactionEntity.class));
	}
	
	@Test
	public void make_notMade() {
		UserEntity user = new UserEntity("email", "password", "name");
		AccountEntity fromAcc = new AccountEntity(user, "fromAcc");
		fromAcc.setBalance(BigDecimal.valueOf(999));
		AccountEntity toAcc = new AccountEntity(user, "toAcc");
		toAcc.setBalance(BigDecimal.valueOf(0));
		CategoryEntity category = new CategoryEntity(user, "cat");
		
		TransactionEntity transaction = new TransactionEntity(BigDecimal.valueOf(1000), new Date(100), fromAcc, toAcc, Collections.singletonList(category));
		
		when(accountRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(fromAcc));
		when(accountRepository.findByIdAndUserId(2, 1)).thenReturn(Optional.of(toAcc));
		when(categoryRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(category));
		when(transactionRepository.save(any(TransactionEntity.class))).thenReturn(transaction);
		
		transactionService.make(1, 2, BigDecimal.valueOf(1000), 1, 1);
		
		verify(transactionRepository, times(0)).save(any(TransactionEntity.class));
	}
}