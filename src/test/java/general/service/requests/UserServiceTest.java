package general.service.requests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import general.service.utils.converter.*;
import general.service.utils.hash.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserServiceTest {
	
	@Autowired
	UserService userService;
	
	@MockBean
	UserRepository userRepository;
	
	@MockBean
	UserEntityToDTO converter;
	
	@MockBean
	Md5Hex digest;
	
	@Test
	public void register_registered() {
		when(digest.hash("password")).thenReturn("hash");
		when(userRepository.save(any(UserEntity.class))).thenReturn(new UserEntity("email", "hash", "name"));
		
		userService.register("email", "password", "name");
		
		verify(userRepository, times(1)).save(any(UserEntity.class));
	}
	
	@Test(expected = Exception.class)
	public void register_notRegistered() {
		when(digest.hash("password")).thenReturn("hash");
		when(userRepository.save(any(UserEntity.class))).thenThrow(Exception.class);
		
		userService.register("email", "password", "name");
		
		verify(userRepository, times(0)).save(any(UserEntity.class));
	}
	
	@Test
	public void findByEmailAndPassword() {
		UserEntity userDAO = new UserEntity("email", "hash", "name");
		when(digest.hash("pass")).thenReturn("hash");
		when(userRepository.findByEmailAndPassword("email", "hash")).thenReturn(Optional.of(userDAO));
		when(converter.convert(userDAO)).thenReturn(new UserDTO(1, "name", "email"));
		
		Optional<UserDTO> user = userService.findByEmailAndPassword("email", "pass");
		
		assertTrue(user.isPresent());
		assertEquals(1, user.get()
							.getId());
		assertEquals("name", user.get()
								 .getName());
		assertEquals("email", user.get()
								  .getEmail());
	}
	
	@Test
	public void findById() {
		UserEntity userDAO = new UserEntity("email", "pass", "name");
		when(userRepository.getById(1)).thenReturn(userDAO);
		when(converter.convert(userDAO)).thenReturn(new UserDTO(1, "name", "email"));
		
		UserDTO user = userService.findById(1);
		
		assertNotNull(user);
		assertEquals("email", user.getEmail());
		assertEquals(1, user.getId());
		assertEquals("name", user.getName());
	}
}