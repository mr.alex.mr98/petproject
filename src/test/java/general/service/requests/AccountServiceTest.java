package general.service.requests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.math.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import general.service.utils.converter.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class AccountServiceTest {
	
	@Autowired
	AccountService accountService;
	
	@MockBean
	AccountRepository accountRepository;
	
	@MockBean
	AccountEntityToDTO converter;
	
	@MockBean
	UserRepository userRepository;
	
	@Test
	public void create_created() {
		UserEntity user = new UserEntity("1", "1", "name");
		AccountEntity accountDAO = new AccountEntity(user, "name");
		
		when(accountRepository.findByNameAndUserId("name", 1)).thenReturn(Optional.empty());
		when(userRepository.getById(1)).thenReturn(user);
		when(accountRepository.save(any(AccountEntity.class))).thenReturn(accountDAO);
		
		accountService.create("name", 1);
		
		verify(accountRepository, times(1)).save(any(AccountEntity.class));
	}
	
	@Test
	public void create_notCreated() {
		UserEntity user = new UserEntity("1", "1", "name");
		AccountEntity accountDAO = new AccountEntity(user, "name");
		
		when(accountRepository.findByNameAndUserId("name", 1)).thenReturn(Optional.of(accountDAO));
		when(userRepository.getById(1)).thenReturn(user);
		when(accountRepository.save(any(AccountEntity.class))).thenReturn(accountDAO);
		
		accountService.create("name", 1);
		
		verify(accountRepository, times(0)).save(any(AccountEntity.class));
	}
	
	@Test
	public void findAll() {
		AccountEntity accountDAO = new AccountEntity(new UserEntity("1", "1", "name"), "acc");
		List<AccountEntity> accounts = Collections.singletonList(accountDAO);
		AccountDTO accountDTO = new AccountDTO(1, "accDTO", BigDecimal.valueOf(1));
		when(converter.convert(accountDAO)).thenReturn(accountDTO);
		when(accountRepository.findAllByUserId(1)).thenReturn(accounts);
		
		List<AccountDTO> accs = accountService.findAll(1);
		
		assertEquals(1, accs.size());
		assertEquals("accDTO", accs.get(0)
								   .getName());
		assertEquals(1, accs.get(0)
							.getId());
	}
	
	@Test
	public void delete_true() {
		AccountEntity accDAO = new AccountEntity(new UserEntity("1", "1", "name"), "acc");
		when(accountRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.of(accDAO));
		
		assertTrue(accountService.delete(1, 1));
	}
	
	@Test
	public void delete_false() {
		when(accountRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.empty());
		
		assertFalse(accountService.delete(1, 1));
	}
	
	@Test
	public void change_true() {
		when(accountRepository.update(1, "name", 1)).thenReturn(1);
		
		assertTrue(accountService.change(1, "name", 1));
	}
	
	@Test
	public void change_false() {
		when(accountRepository.update(1, "name", 1)).thenReturn(0);
		
		assertFalse(accountService.change(1, "name", 1));
	}
}