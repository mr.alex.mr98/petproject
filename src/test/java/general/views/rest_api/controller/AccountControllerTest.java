package general.views.rest_api.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.http.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class AccountControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	AccountService accountService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void userAccounts() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(get("/api/accounts"))
			   .andExpect(status().isOk())
			   .andExpect(content().json(
				   "[\n" +
				   "  {\n" +
				   "    \"id\": 1,  \n" +
				   "    \"name\": \"acc\",\n" +
				   "    \"balance\": 1000\n" +
				   "  }\n" +
				   "]  \n" +
				   "\n"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void deleteAccount_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.delete(1, 1)).thenReturn(true);
		
		mockMvc.perform(post("/api/delete-account")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"accountId\": 1\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have deleted the account!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void deleteAccount_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.delete(1, 1)).thenReturn(false);
		
		mockMvc.perform(post("/api/delete-account"))
			   .andExpect(status().isBadRequest());
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void changeAccount_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.change(1, "1", 1)).thenReturn(true);
		
		mockMvc.perform(post("/api/change-account")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"accountId\": 1,\n" +
									 "    \"newName\": \"1\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have changed the account!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void changeAccount_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.change(1, "1", 1)).thenReturn(false);
		
		mockMvc.perform(post("/api/change-account"))
			   .andExpect(status().isBadRequest());
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void createAccount_created() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.create("1", 1)).thenReturn(Optional.of(new AccountDTO(1, "1", BigDecimal.valueOf(0))));
		
		mockMvc.perform(post("/api/create-account")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"name\": \"1\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have created the account!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void createAccount_notCreated() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.create("1", 1)).thenReturn(Optional.empty());
		
		mockMvc.perform(post("/api/create-account"))
			   .andExpect(status().isBadRequest());
	}
}