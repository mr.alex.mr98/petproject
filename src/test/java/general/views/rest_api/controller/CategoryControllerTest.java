package general.views.rest_api.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.http.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.sql.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class CategoryControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	CategoryService categoryService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void userCategories() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.findAll(1)).thenReturn(Collections.singletonList(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(get("/api/categories"))
			   .andExpect(status().isOk())
			   .andExpect(content().json(
				   "[\n" +
				   "  {\n" +
				   "  \"id\": 1,\n" +
				   "  \"name\": \"cat\"\n" +
				   "  }\n" +
				   "]  \n" +
				   "\n"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void deleteCategory_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.delete(1, 1)).thenReturn(true);
		
		mockMvc.perform(post("/api/delete-category")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"categoryId\": 1\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have deleted the category!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void deleteCategory_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.delete(1, 1)).thenReturn(false);
		
		mockMvc.perform(post("/api/delete-category"))
			   .andExpect(status().isBadRequest());
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void changeCategory_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.change(1, "1", 1)).thenReturn(true);
		
		mockMvc.perform(post("/api/change-category")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"categoryId\": 1,\n" +
									 "    \"newName\": \"1\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have changed the category!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void changeCategory_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.change(1, "1", 1)).thenReturn(false);
		
		mockMvc.perform(post("/api/change-category"))
			   .andExpect(status().isBadRequest());
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void createCategory_created() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.create("1", 1)).thenReturn(Optional.of(new CategoryDTO(1, "1")));
		
		mockMvc.perform(post("/api/create-category")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"name\": \"1\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": " +
										 "\"You have created the category!\"\n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void createCategory_notCreated() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.create("1", 1)).thenReturn(Optional.empty());
		
		mockMvc.perform(post("/api/create-category"))
			   .andExpect(status().isBadRequest());
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void categoriesReport_correctRequest() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.report(Timestamp.valueOf("2022-01-01 00:00:01"), Timestamp.valueOf("2022-01-05 23:59:59"), 1))
			.thenReturn(Collections.singletonList(new ReportDTO("cat", 1000)));
		
		mockMvc.perform(post("/api/report")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"from\": \"2022-01-01\",\n" +
									 "    \"till\": \"2022-01-05\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("[\n" +
										 "  {\n" +
										 "    \"catName\": \"cat\",\n" +
										 "    \"value\": 1000\n" +
										 "  }\n" +
										 "]"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void categoriesReport_incorrectRequest() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.report(Timestamp.valueOf("2022-01-01 00:00:01"), Timestamp.valueOf("2022-01-05 23:59:59"), 1))
			.thenReturn(Collections.emptyList());
		
		mockMvc.perform(post("/api/report")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"from\": \"2022\",\n" +
									 "    \"till\": \"2022\"\n" +
									 "}"))
			   .andExpect(status().isBadRequest());
	}
}