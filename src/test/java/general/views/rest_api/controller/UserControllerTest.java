package general.views.rest_api.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.http.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class UserControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getUserInfo() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		
		mockMvc.perform(get("/api/user-info"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"id\": 1,\n" +
										 "  \"name\": \"test\",\n" +
										 "  \"email\": \"test@test.com\"\n" +
										 "}"));
	}
	
	@Test
	public void registration_made() throws Exception {
		
		when(userService.register("1", "1", "1")).thenReturn(Optional.of(new UserDTO(1, "name", "email")));
		
		mockMvc.perform(post("/api/register")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"email\": \"1\",\n" +
									 "    \"password\": \"1\",\n" +
									 "    \"name\": \"1\"\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"done\": \"Registration has been done!\"" +
										 " \n" +
										 "}"));
	}
	
	@Test
	public void registration_notMade() throws Exception {
		
		when(userService.register("1", "1", "1")).thenReturn(Optional.empty());
		
		mockMvc.perform(post("/api/register"))
			   .andExpect(status().isBadRequest());
	}
}