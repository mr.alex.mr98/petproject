package general.views.rest_api.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.http.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class TransactionControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	TransactionService transactionService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void makeTransaction_made() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(transactionService.make(1, 2, BigDecimal.valueOf(10), 1, 1))
			.thenReturn(Optional.of(new TransactionDTO(BigDecimal.valueOf(10),
													   new Date(System.currentTimeMillis()),
													   1,
													   2)));
		
		mockMvc.perform(post("/api/transaction")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\n" +
									 "    \"fromAccountId\": 1,\n" +
									 "    \"toAccountId\": 2,\n" +
									 "    \"value\": 10,\n" +
									 "    \"categoryId\": 1\n" +
									 "}"))
			   .andExpect(status().isOk())
			   .andExpect(content().json("{\n" +
										 "  \"message\": \"You have made the transaction!\"" +
										 " \n" +
										 "}"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void makeTransaction_notMade() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(transactionService.make(1, 2, BigDecimal.valueOf(10), 1, 1))
			.thenReturn(Optional.empty());
		
		mockMvc.perform(post("/api/transaction"))
			   .andExpect(status().isBadRequest());
	}
}