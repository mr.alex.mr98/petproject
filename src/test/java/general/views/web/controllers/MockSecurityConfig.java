package general.views.web.controllers;

import static java.util.Collections.*;
import static general.security.UserRole.*;

import org.springframework.context.annotation.*;
import org.springframework.security.core.userdetails.*;

import java.util.stream.*;

import general.security.*;


@Configuration
public class MockSecurityConfig {
	
	@Bean
	public UserDetailsService userDetailsService() {
		return u -> new CustomUserDetails(1,
										  "test@test.com",
										  "test",
										  singleton(USER).stream()
														 .map(CustomGrantedAuthority::new)
														 .collect(Collectors.toList()));
	}
}
