package general.views.web.controllers.accounts;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.*;


@RunWith(SpringRunner.class)
@WebMvcTest(CreateAccountController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class CreateAccountControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AccountService accountService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getCreateAcc() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		
		mockMvc.perform(get("/create-account"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("createByNameForm", new CreateByNameForm()))
			   .andExpect(view().name("/accounts/create_acc"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postCreateAcc_created() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.create(null, 1)).thenReturn(Optional.of(new AccountDTO(1, "acc", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(post("/create-account"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/accounts"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteAcc_notCreated() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.create(null, 1)).thenReturn(Optional.empty());
		
		mockMvc.perform(post("/create-account"))
			   .andExpect(status().isOk())
			   .andExpect(view().name("/accounts/create_acc"));
	}
}