package general.views.web.controllers.accounts;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.*;


@RunWith(SpringRunner.class)
@WebMvcTest(ChangeAccountController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class ChangeAccountControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AccountService accountService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getChangeAcc() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(get("/change-account"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(1000)))))
			   .andExpect(model().attribute("changeNameForm", new ChangeNameByIdForm()))
			   .andExpect(view().name("/accounts/change_acc_name"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postChangeAcc_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.change(0, null, 1)).thenReturn(true);
		
		mockMvc.perform(post("/change-account"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/accounts"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postChangeAcc_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.change(0, null, 1)).thenReturn(false);
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(post("/change-account"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(1000)))))
			   .andExpect(model().attributeHasErrors("changeNameForm"))
			   .andExpect(view().name("/accounts/change_acc_name"));
	}
}