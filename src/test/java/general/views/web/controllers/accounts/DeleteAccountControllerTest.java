package general.views.web.controllers.accounts;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.*;


@RunWith(SpringRunner.class)
@WebMvcTest(DeleteAccountController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class DeleteAccountControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AccountService accountService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getDeleteAcc() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "cat", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(get("/delete-account"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new AccountDTO(1, "cat", BigDecimal.valueOf(1000)))))
			   .andExpect(model().attribute("deleteByIdForm", new RequestByIdForm()))
			   .andExpect(view().name("/accounts/delete_acc"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteAcc_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.delete(0, 1)).thenReturn(true);
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "cat", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(post("/delete-account"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/accounts"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteCat_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.delete(0, 1)).thenReturn(false);
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "cat", BigDecimal.valueOf(1000))));
		
		mockMvc.perform(post("/delete-account"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new AccountDTO(1, "cat", BigDecimal.valueOf(1000)))))
			   .andExpect(model().attributeHasErrors("deleteByIdForm"))
			   .andExpect(view().name("/accounts/delete_acc"));
	}
}