package general.views.web.controllers.user;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.forms.user.*;


@RunWith(SpringRunner.class)
@WebMvcTest(RegisterController.class)
@Import({SecurityConfig.class})
public class RegisterControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@Test
	public void getRegister() throws Exception {
		mockMvc.perform(get("/register"))
			   .andExpect(status().isOk())
			   .andExpect(view().name("register"));
	}
	
	@Test
	public void postRegister_registered() throws Exception {
		when(userService.register(null, null, null)).
			thenReturn(Optional.of(new UserDTO(1, "test", "test")));
		
		mockMvc.perform(post("/register"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/login-form"));
	}
	
	@Test
	public void postRegister_notRegistered() throws Exception {
		when(userService.register(null, null, null)).
			thenReturn(Optional.empty());
		
		mockMvc.perform(post("/register"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("regForm", new RegistrationForm()))
			   .andExpect(model().attributeHasErrors("regForm"))
			   .andExpect(view().name("register"));
	}
}