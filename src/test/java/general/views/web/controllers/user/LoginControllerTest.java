package general.views.web.controllers.user;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class LoginControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void index() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		
		mockMvc.perform(get("/personal-area"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("name", "test"))
			   .andExpect(view().name("personal-area"));
	}
	
	@Test
	public void getAuth() throws Exception {
		mockMvc.perform(get("/login-form"))
			   .andExpect(status().isOk())
			   .andExpect(view().name("login-form"));
	}
}