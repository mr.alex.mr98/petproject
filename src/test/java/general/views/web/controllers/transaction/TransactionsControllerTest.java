package general.views.web.controllers.transaction;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.math.*;
import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.transactions.*;


@RunWith(SpringRunner.class)
@WebMvcTest(TransactionsController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class TransactionsControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	TransactionService transactionService;
	
	@MockBean
	AccountService accountService;
	
	@MockBean
	CategoryService categoryService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getTransaction() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(accountService.findAll(1)).thenReturn(Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(100000))));
		when(categoryService.findAll(1)).thenReturn(Collections.singletonList(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(get("/transaction"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("transactionForm", new TransactionForm()))
			   .andExpect(model().attribute("accList", Collections.singletonList(new AccountDTO(1, "acc", BigDecimal.valueOf(100000)))))
			   .andExpect(model().attribute("catList", Collections.singletonList(new CategoryDTO(1, "cat"))))
			   .andExpect(view().name("/transaction/transaction"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postTransaction_made() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(transactionService.make(0, 0, null, 0, 1))
			.thenReturn(Optional.of(new TransactionDTO(BigDecimal.valueOf(1000),
													   new Date(System.currentTimeMillis()),
													   1,
													   2)));
		
		mockMvc.perform(post("/transaction"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/accounts"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postTransaction_notMade() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(transactionService.make(0, 0, null, 0, 1))
			.thenReturn(Optional.empty());
		
		mockMvc.perform(post("/transaction"))
			   .andExpect(status().isOk())
			   .andExpect(model().attributeHasErrors("transactionForm"))
			   .andExpect(view().name("/transaction/transaction"));
	}
}