package general.views.web.controllers.category;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.*;


@RunWith(SpringRunner.class)
@WebMvcTest(CreateCategoryController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class CreateCategoryControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	CategoryService categoryService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getCreateCat() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		
		mockMvc.perform(get("/create-category"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("createByNameForm", new CreateByNameForm()))
			   .andExpect(view().name("/categories/create_cat"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postCreateCat_created() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.create(null, 1)).thenReturn(Optional.of(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(post("/create-category"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/categories"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteCat_notCreated() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.create(null, 1)).thenReturn(Optional.empty());
		
		mockMvc.perform(post("/create-category"))
			   .andExpect(status().isOk())
			   .andExpect(view().name("/categories/create_cat"));
	}
}