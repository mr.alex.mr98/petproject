package general.views.web.controllers.category;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.*;


@RunWith(SpringRunner.class)
@WebMvcTest(DeleteCategoryController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class DeleteCategoryControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	CategoryService categoryService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getDeleteCat() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.findAll(1)).thenReturn(Collections.singletonList(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(get("/delete-category"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new CategoryDTO(1, "cat"))))
			   .andExpect(model().attribute("deleteByIdForm", new RequestByIdForm()))
			   .andExpect(view().name("/categories/delete_cat"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteCat_true() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.delete(0, 1)).thenReturn(true);
		when(categoryService.findAll(1)).thenReturn(Collections.singletonList(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(post("/delete-category"))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(redirectedUrl("/categories"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postDeleteCat_false() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.delete(0, 1)).thenReturn(false);
		when(categoryService.findAll(1)).thenReturn(Collections.singletonList(new CategoryDTO(1, "cat")));
		
		mockMvc.perform(post("/delete-category"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("list", Collections.singletonList(new CategoryDTO(1, "cat"))))
			   .andExpect(model().attributeHasErrors("deleteByIdForm"))
			   .andExpect(view().name("/categories/delete_cat"));
	}
}