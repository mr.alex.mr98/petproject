package general.views.web.controllers.report;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.context.annotation.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;

import java.util.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import general.views.web.controllers.*;
import general.views.web.forms.report.*;


@RunWith(SpringRunner.class)
@WebMvcTest(ReportController.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
public class ReportControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	CategoryService categoryService;
	
	@MockBean
	UserService userService;
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void getReport() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		
		mockMvc.perform(get("/report"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("reportForm", new ReportForm()))
			   .andExpect(model().attribute("list", new ArrayList<>(0)))
			   .andExpect(view().name("/report/report"));
	}
	
	@WithUserDetails(value = "test@test.com", userDetailsServiceBeanName = "userDetailsService")
	@Test
	public void postReport_correct() throws Exception {
		when(userService.findById(1)).thenReturn(new UserDTO(1, "test", "test@test.com"));
		when(categoryService.report(null, null, 1)).thenReturn(Collections.singletonList(new ReportDTO("cat", "value")));
		
		mockMvc.perform(post("/report"))
			   .andExpect(status().isOk())
			   .andExpect(model().attribute("reportForm", new ReportForm()))
			   .andExpect(model().attribute("list", new ArrayList<>(0)))
			   .andExpect(view().name("/report/report"));
	}
}