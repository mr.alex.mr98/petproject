package general.dao.repository;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.user.*;
import general.security.*;


@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserRepositoryTest {
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void findByFilter_found() {
		UserFilter filter = new UserFilter().setEmailLike("test%")
											.setUserRole(UserRole.USER);
		
		List<UserEntity> users = userRepository.findByFilter(filter);
		
		assertNotNull(users);
		assertEquals(1, users.size());
	}
	
	@Test
	public void findByFilter_notFound() {
		UserFilter filter = new UserFilter().setEmailLike("notExisting%")
											.setUserRole(UserRole.USER);
		
		List<UserEntity> users = userRepository.findByFilter(filter);
		
		assertNotNull(users);
		assertEquals(0, users.size());
	}
}