package general.dao.repository;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.category.*;


@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class CategoryRepositoryTest {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Test
	public void findByFilter_found() {
		CategoryFilter filter = new CategoryFilter().setNameLike("%")
													.setUserId(1);
		
		List<CategoryEntity> categories = categoryRepository.findByFilter(filter);
		
		assertNotNull(categories);
		assertEquals(3, categories.size());
	}
	
	@Test
	public void findByFilter_notFound() {
		CategoryFilter filter = new CategoryFilter().setNameLike("notExisting%")
													.setUserId(1);
		
		List<CategoryEntity> categories = categoryRepository.findByFilter(filter);
		
		assertNotNull(categories);
		assertEquals(0, categories.size());
	}
}