package general.dao.repository;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.math.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.account.*;


@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class AccountRepositoryTest {
	
	@Autowired
	AccountRepository accountRepository;
	
	@Test
	public void findByFilter_found() {
		AccountFilter filter = new AccountFilter().setBalance(BigDecimal.valueOf(1000))
												  .setNameLike("%")
												  .setUserId(1);
		
		List<AccountEntity> accounts = accountRepository.findByFilter(filter);
		
		assertNotNull(accounts);
		assertEquals(2, accounts.size());
	}
	
	@Test
	public void findByFilter_notFound() {
		AccountFilter filter = new AccountFilter().setBalance(BigDecimal.valueOf(9999999))
												  .setNameLike("%")
												  .setUserId(1);
		
		List<AccountEntity> accounts = accountRepository.findByFilter(filter);
		
		assertNotNull(accounts);
		assertEquals(0, accounts.size());
	}
}