package general.service.utils.converter;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

import general.service.entity.*;


@Service
public class ReportObjToDTO implements Converter<Object[], ReportDTO> {
	
	@Override
	public ReportDTO convert(Object[] source) {
		return new ReportDTO(source[0], source[1]);
	}
}
