package general.service.utils.converter;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

import general.dao.entity.*;
import general.service.entity.*;


@Service
public class AccountEntityToDTO implements Converter<AccountEntity, AccountDTO> {
	
	@Override
	public AccountDTO convert(AccountEntity source) {
		return new AccountDTO(source.getId(), source.getName(), source.getBalance());
	}
}
