package general.service.utils.converter;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

import general.dao.entity.*;
import general.service.entity.*;


@Service
public class TransactionEntityToDTO implements Converter<TransactionEntity, TransactionDTO> {
	
	@Override
	public TransactionDTO convert(TransactionEntity source) {
		return new TransactionDTO(source.getValue(),
								  source.getDate(),
								  source.getFromAccountEntity()
										.getId(),
								  source.getToAccountEntity()
										.getId());
	}
}
