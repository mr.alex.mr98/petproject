package general.service.utils.converter;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

import general.dao.entity.*;
import general.service.entity.*;


@Service
public class CategoryEntityToDTO implements Converter<CategoryEntity, CategoryDTO> {
	
	@Override
	public CategoryDTO convert(CategoryEntity source) {
		return new CategoryDTO(source.getId(), source.getName());
	}
}
