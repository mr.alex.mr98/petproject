package general.service.utils.converter;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

import general.dao.entity.*;
import general.service.entity.*;


@Service
public class UserEntityToDTO implements Converter<UserEntity, UserDTO> {
	
	@Override
	public UserDTO convert(UserEntity source) {
		return new UserDTO(source.getId(), source.getName(), source.getEmail());
	}
}
