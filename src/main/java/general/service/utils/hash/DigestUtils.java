package general.service.utils.hash;

public interface DigestUtils<S, T> {
	
	T hash(S source);
}
