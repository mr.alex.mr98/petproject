package general.service.utils.hash;

import org.springframework.stereotype.*;


@Service
public class Md5Hex implements DigestUtils<String, String> {
	
	@Override
	public String hash(String source) {
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(source);
	}
}
