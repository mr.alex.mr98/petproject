package general.service.entity;

import java.math.*;
import java.util.*;

import lombok.*;


@ToString
@Data
@AllArgsConstructor
public class TransactionDTO {
	
	private BigDecimal value;
	private Date date;
	private int fromAccId;
	private int toAccId;
}
