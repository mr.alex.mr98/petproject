package general.service.entity;

import lombok.*;


@ToString
@Data
@AllArgsConstructor
public class CategoryDTO {
	
	private int id;
	private String name;
}
