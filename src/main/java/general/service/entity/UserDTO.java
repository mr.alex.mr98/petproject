package general.service.entity;

import lombok.*;


@ToString
@Data
@AllArgsConstructor
public class UserDTO {
	
	private int id;
	private String name;
	private String email;
}



