package general.service.entity;

import lombok.*;


@ToString
@Data
@AllArgsConstructor
public class ReportDTO {
	
	private Object catName;
	private Object value;
}
