package general.service.entity;

import java.math.*;

import lombok.*;


@ToString
@Data
@AllArgsConstructor
public class AccountDTO {
	
	private int id;
	private String name;
	private BigDecimal balance;
}
