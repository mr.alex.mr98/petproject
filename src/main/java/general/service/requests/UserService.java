package general.service.requests;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import general.service.exception.*;
import general.service.utils.hash.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class UserService {
	
	private final UserRepository userRepository;
	private final Converter<UserEntity, UserDTO> converter;
	private final Md5Hex digest;
	
	@Transactional
	public Optional<UserDTO> register(String email, String password, String name) {
		try {
			UserEntity user = new UserEntity(email,
											 digest.hash(password),
											 name);
			userRepository.save(user);
			return Optional.ofNullable(converter.convert(user));
		} catch (Exception e) {
			throw new CustomException(e);
		}
	}
	
	public Optional<UserDTO> findByEmailAndPassword(String email, String password) {
		return userRepository.findByEmailAndPassword(email, digest.hash(password))
							 .map(converter::convert);
	}
	
	public UserDTO findById(int userId) {
		return converter.convert(userRepository.getById(userId));
	}
}
