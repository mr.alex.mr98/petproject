package general.service.requests;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.sql.*;
import java.util.*;
import java.util.stream.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class CategoryService {
	
	private final UserRepository userRepository;
	private final CategoryRepository categoryRepository;
	private final Converter<CategoryEntity, CategoryDTO> converter;
	private final Converter<Object[], ReportDTO> reportConverter;
	
	public List<CategoryDTO> findAll(int userId) {
		return categoryRepository.findAllByUserId(userId)
								 .stream()
								 .map(converter::convert)
								 .collect(Collectors.toList());
	}
	
	@Transactional
	public boolean delete(int catId, int userId) {
		try {
			categoryRepository.delete(categoryRepository.findByIdAndUserId(catId, userId)
														.get());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Transactional
	public boolean change(int catId, String newName, int userId) {
		return categoryRepository.update(catId, newName, userId) == 1;
	}
	
	@Transactional
	public Optional<CategoryDTO> create(String name, int userId) {
		try {
			if (categoryRepository.findByNameAndUserId(name, userId)
								  .isPresent()) {
				return Optional.empty();
			}
			
			CategoryEntity category = new CategoryEntity(userRepository.getById(userId), name);
			categoryRepository.save(category);
			
			return Optional.ofNullable(converter.convert(category));
		} catch (Exception e) {
			return Optional.empty();
		}
	}
	
	public List<ReportDTO> report(Timestamp from, Timestamp till, int userId) {
		return categoryRepository.reportOnPeriod(from, till, userId)
								 .stream()
								 .map(reportConverter::convert)
								 .collect(Collectors.toList());
	}
}
