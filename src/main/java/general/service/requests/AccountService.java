package general.service.requests;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.math.*;
import java.util.*;
import java.util.stream.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class AccountService {
	
	private final AccountRepository accountRepository;
	private final TransactionRepository transactionRepository;
	private final UserRepository userRepository;
	private final Converter<AccountEntity, AccountDTO> converter;
	
	@Transactional
	public boolean delete(int accId, int userId) {
		try {
			if (accountRepository.findByIdAndUserId(accId, userId)
								 .get()
								 .getBalance()
								 .compareTo(BigDecimal.ZERO) > 0) {
				return false;
			}
			
			List<TransactionEntity> transactions = transactionRepository.findAllByFromAccountEntityId(accId);
			transactions.forEach(t -> t.setFromAccountEntity(null));
			transactionRepository.saveAll(transactions);
			
			transactions = transactionRepository.findAllByToAccountEntityId(accId);
			transactions.forEach(t -> t.setToAccountEntity(null));
			
			transactionRepository.saveAll(transactions);
			accountRepository.deleteById(accId);
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public List<AccountDTO> findAll(int userId) {
		return accountRepository.findAllByUserId(userId)
								.stream()
								.map(converter::convert)
								.collect(Collectors.toList());
	}
	
	@Transactional
	public boolean change(int accId, String newName, int userId) {
		return accountRepository.update(accId, newName, userId) == 1;
	}
	
	@Transactional
	public Optional<AccountDTO> create(String name, int userId) {
		try {
			if (accountRepository.findByNameAndUserId(name, userId)
								 .isPresent()) {
				return Optional.empty();
			}
			
			AccountEntity newAccount = new AccountEntity(userRepository.getById(userId),
														 name);
			accountRepository.save(newAccount);
			
			return Optional.ofNullable(converter.convert(newAccount));
		} catch (Exception e) {
			return Optional.empty();
		}
	}
}
