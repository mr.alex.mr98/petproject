package general.service.requests;

import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.math.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.*;
import general.service.entity.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class TransactionService {
	
	private final TransactionRepository transactionRepository;
	private final AccountRepository accountRepository;
	private final CategoryRepository categoryRepository;
	private final Converter<TransactionEntity, TransactionDTO> converter;
	
	@Transactional
	public Optional<TransactionDTO> make(Integer fromId, Integer toId, BigDecimal value, Integer catId, int userId) {
		try {
			AccountEntity fromAccount = accountRepository.findByIdAndUserId(fromId, userId)
														 .get();
			AccountEntity toAccount = accountRepository.findByIdAndUserId(toId, userId)
													   .get();
			CategoryEntity category = categoryRepository.findByIdAndUserId(catId, userId)
														.get();
			
			if (category == null ||
				fromAccount.getBalance()
						   .compareTo(value) < 0) {
				return Optional.empty();
			}
			
			fromAccount.setBalance(fromAccount.getBalance()
											  .subtract(value));
			toAccount.setBalance(toAccount.getBalance()
										  .add(value));
			TransactionEntity transaction = new TransactionEntity(value,
																  new Date(System.currentTimeMillis()),
																  fromAccount,
																  toAccount,
																  Collections.singletonList(category));
			
			transactionRepository.save(transaction);
			
			return Optional.ofNullable(converter.convert(transaction));
		} catch (Exception e) {
			return Optional.empty();
		}
	}
}
