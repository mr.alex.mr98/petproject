package general.dao.entity;

import java.math.*;

import javax.persistence.*;

import lombok.*;


@Entity
@Table(name = "account")
@Getter
@Setter
public class AccountEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "balance")
	private BigDecimal balance;
	
	@ManyToOne()
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	public AccountEntity(UserEntity user, String name) {
		balance = BigDecimal.valueOf(0);
		this.user = user;
		this.name = name;
	}
	
	public AccountEntity() {
	}
}
