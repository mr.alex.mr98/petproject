package general.dao.entity;

import java.util.*;

import javax.persistence.*;

import lombok.*;


@Getter
@Setter
@Entity
@Table(name = "category")
public class CategoryEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@ManyToMany(cascade = {CascadeType.PERSIST})
	@JoinTable(name = "transaction_to_category",
			   joinColumns = @JoinColumn(name = "category_id"),
			   inverseJoinColumns = @JoinColumn(name = "transaction_id"))
	private List<TransactionEntity> transactions = new ArrayList<>();
	
	public CategoryEntity(UserEntity user, String name) {
		this.user = user;
		this.name = name;
	}
	
	public CategoryEntity() {
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, name, user, transactions);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CategoryEntity category = (CategoryEntity) o;
		return id == category.id && Objects.equals(name, category.name) && Objects.equals(user, category.user) && Objects.equals(transactions, category.transactions);
	}
}
