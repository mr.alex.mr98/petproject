package general.dao.entity;

import java.math.*;
import java.util.*;

import javax.persistence.*;

import lombok.*;


@Getter
@Setter
@Entity
@Table(name = "transactions")
public class TransactionEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "volume")
	private BigDecimal value;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "from_account_id")
	private AccountEntity fromAccountEntity;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "to_account_id")
	private AccountEntity toAccountEntity;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(name = "transaction_to_category",
			   joinColumns = @JoinColumn(name = "transaction_id"),
			   inverseJoinColumns = @JoinColumn(name = "category_id"))
	private List<CategoryEntity> category = new ArrayList<>();
	
	public TransactionEntity(BigDecimal value,
							 Date date,
							 AccountEntity fromAccountEntity,
							 AccountEntity toAccountEntity,
							 List<CategoryEntity> category) {
		this.value = value;
		this.date = date;
		this.fromAccountEntity = fromAccountEntity;
		this.toAccountEntity = toAccountEntity;
		this.category = category;
	}
	
	public TransactionEntity() {
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, value, date, fromAccountEntity, toAccountEntity, category);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TransactionEntity that = (TransactionEntity) o;
		return id == that.id && Objects.equals(value, that.value) && Objects.equals(date, that.date) && Objects.equals(fromAccountEntity, that.fromAccountEntity) && Objects.equals(toAccountEntity, that.toAccountEntity) && Objects.equals(category, that.category);
	}
}
