package general.dao.entity;

import static java.util.Collections.*;

import java.util.*;

import javax.persistence.*;

import general.security.*;
import lombok.*;


@Getter
@Setter
@Entity
@Table(name = "maintainer")
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
	@Column(name = "role")
	private Set<UserRole> roles = emptySet();
	
	public UserEntity(String email, String password, String name) {
		this.name = name;
		this.email = email;
		this.password = password;
	}
	
	public UserEntity() {
	}
}



