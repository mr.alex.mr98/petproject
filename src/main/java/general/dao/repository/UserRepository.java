package general.dao.repository;

import org.springframework.data.jpa.repository.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.user.*;


public interface UserRepository extends JpaRepository<UserEntity, Integer>, UserRepositoryCustom {
	
	Optional<UserEntity> findByEmail(String email);
	Optional<UserEntity> findByEmailAndPassword(String email, String password);
}
