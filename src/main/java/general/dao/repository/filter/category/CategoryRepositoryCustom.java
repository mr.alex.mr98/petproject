package general.dao.repository.filter.category;

import java.util.*;

import general.dao.entity.*;


public interface CategoryRepositoryCustom {
	
	List<CategoryEntity> findByFilter(CategoryFilter filter);
}
