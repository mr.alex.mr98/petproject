package general.dao.repository.filter.category;

import java.util.*;

import javax.persistence.*;

import general.dao.entity.*;
import lombok.*;


@RequiredArgsConstructor
public class CategoryRepositoryImpl implements CategoryRepositoryCustom {
	
	private final EntityManager em;
	
	@Override
	public List<CategoryEntity> findByFilter(CategoryFilter filter) {
		String query = "select c from CategoryEntity c where 1 = 1";
		
		Map<String, Object> params = new HashMap<>();
		
		if (filter.getNameLike() != null) {
			query += " and name like :nameLike";
			params.put("nameLike", filter.getNameLike());
		}
		
		if (filter.getUserId() != null) {
			query += " and c.user.id = :userId";
			params.put("userId", filter.getUserId());
		}
		
		TypedQuery<CategoryEntity> typedQuery = em.createQuery(query, CategoryEntity.class);
		
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			typedQuery.setParameter(entry.getKey(), entry.getValue());
		}
		
		return typedQuery.getResultList();
	}
}
