package general.dao.repository.filter.category;

import lombok.*;
import lombok.experimental.*;


@Accessors(chain = true)
@Data
public class CategoryFilter {
	
	private String nameLike;
	private Integer userId;
}
