package general.dao.repository.filter.user;

import general.security.*;
import lombok.*;
import lombok.experimental.*;


@Accessors(chain = true)
@Data
public class UserFilter {
	
	private String emailLike;
	private UserRole userRole;
}
