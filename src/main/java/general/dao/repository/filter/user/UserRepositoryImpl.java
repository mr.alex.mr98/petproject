package general.dao.repository.filter.user;

import java.util.*;

import javax.persistence.*;

import general.dao.entity.*;
import lombok.*;


@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepositoryCustom {
	
	private final EntityManager em;
	
	@Override
	public List<UserEntity> findByFilter(UserFilter filter) {
		String query = "select u from UserEntity u where 1 = 1";
		
		Map<String, Object> params = new HashMap<>();
		
		if (filter.getEmailLike() != null) {
			query += " and email like :emailLike";
			params.put("emailLike", filter.getEmailLike());
		}
		
		if (filter.getUserRole() != null) {
			query += " and :role member of u.roles";
			params.put("role", filter.getUserRole());
		}
		
		TypedQuery<UserEntity> typedQuery = em.createQuery(query, UserEntity.class);
		
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			typedQuery.setParameter(entry.getKey(), entry.getValue());
		}
		
		return typedQuery.getResultList();
	}
}
