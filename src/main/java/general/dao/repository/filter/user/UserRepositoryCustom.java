package general.dao.repository.filter.user;

import java.util.*;

import general.dao.entity.*;


public interface UserRepositoryCustom {
	
	List<UserEntity> findByFilter(UserFilter filter);
}
