package general.dao.repository.filter.account;

import java.util.*;

import javax.persistence.*;

import general.dao.entity.*;
import lombok.*;


@RequiredArgsConstructor
public class AccountRepositoryImpl implements AccountRepositoryCustom {
	
	private final EntityManager em;
	
	@Override
	public List<AccountEntity> findByFilter(AccountFilter filter) {
		String query = "select a from AccountEntity a where 1 = 1";
		
		Map<String, Object> params = new HashMap<>();
		
		if (filter.getNameLike() != null) {
			query += " and name like :nameLike";
			params.put("nameLike", filter.getNameLike());
		}
		
		if (filter.getUserId() != null) {
			query += " and a.user.id = :userId";
			params.put("userId", filter.getUserId());
		}
		
		if (filter.getBalance() != null) {
			query += " and balance >= :balance";
			params.put("balance", filter.getBalance());
		}
		
		TypedQuery<AccountEntity> typedQuery = em.createQuery(query, AccountEntity.class);
		
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			typedQuery.setParameter(entry.getKey(), entry.getValue());
		}
		
		return typedQuery.getResultList();
	}
}
