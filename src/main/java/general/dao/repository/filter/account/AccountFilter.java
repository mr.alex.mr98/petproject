package general.dao.repository.filter.account;

import java.math.*;

import lombok.*;
import lombok.experimental.*;


@Accessors(chain = true)
@Data
public class AccountFilter {
	
	private String nameLike;
	private Integer userId;
	private BigDecimal balance;
}
