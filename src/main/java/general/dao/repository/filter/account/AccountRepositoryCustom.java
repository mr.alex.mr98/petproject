package general.dao.repository.filter.account;

import java.util.*;

import general.dao.entity.*;


public interface AccountRepositoryCustom {
	
	List<AccountEntity> findByFilter(AccountFilter filter);
}
