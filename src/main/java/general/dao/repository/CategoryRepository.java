package general.dao.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;

import java.sql.*;
import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.category.*;


public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer>, CategoryRepositoryCustom {
	
	List<CategoryEntity> findAllByUserId(int userId);
	Optional<CategoryEntity> findByNameAndUserId(String catName, int userId);
	Optional<CategoryEntity> findByIdAndUserId(int categoryId, int userId);
	
	@Query("select c.name, sum(t.value) from CategoryEntity c join c.transactions t join c.user u " +
		   "where t.date >= :from and t.date <= :till and u.id = :userId group by c.name")
	List<Object[]> reportOnPeriod(@Param("from") Timestamp from,
								  @Param("till") Timestamp till,
								  @Param("userId") int userId);
	
	@Modifying
	@Query("update CategoryEntity c set c.name = :newName where c.id = :catId and c.user.id = :userId")
	int update(@Param("catId") int catId,
			   @Param("newName") String newName,
			   @Param("userId") int userId);
}
