package general.dao.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;

import java.util.*;

import general.dao.entity.*;
import general.dao.repository.filter.account.*;


public interface AccountRepository extends JpaRepository<AccountEntity, Integer>, AccountRepositoryCustom {
	
	List<AccountEntity> findAllByUserId(int userId);
	Optional<AccountEntity> findByNameAndUserId(String accName, int userId);
	Optional<AccountEntity> findByIdAndUserId(int accountId, int userId);
	
	@Modifying
	@Query("update AccountEntity a set a.name = :newName where a.id = :accId and a.user.id = :userId")
	int update(@Param("accId") int accId,
			   @Param("newName") String newName,
			   @Param("userId") int userId);
}
