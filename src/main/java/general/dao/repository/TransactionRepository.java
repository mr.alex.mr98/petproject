package general.dao.repository;

import org.springframework.data.jpa.repository.*;

import java.util.*;

import general.dao.entity.*;


public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {
	
	List<TransactionEntity> findAllByFromAccountEntityId(int accountId);
	List<TransactionEntity> findAllByToAccountEntityId(int accountId);
}
