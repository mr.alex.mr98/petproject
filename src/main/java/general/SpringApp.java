package general;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.transaction.annotation.*;


@SpringBootApplication
@EnableTransactionManagement
@EnableWebSecurity
public class SpringApp {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringApp.class, args);
	}
}
