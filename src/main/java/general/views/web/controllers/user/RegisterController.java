package general.views.web.controllers.user;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.user.*;


@Controller
public class RegisterController extends AbstractController {
	
	@Autowired
	public RegisterController(UserService userService) {
		super(userService);
	}
	
	@GetMapping("/register")
	public String getRegister(Model model) {
		model.addAttribute("regForm", new RegistrationForm());
		return "register";
	}
	
	@PostMapping("/register")
	public String postRegister(@ModelAttribute("regForm") @Valid RegistrationForm form,
							   BindingResult bind,
							   Model model) {
		model.addAttribute("regForm", form);
		
		if (!userService.register(form.getEmail(), form.getPassword(), form.getName())
						.isPresent()) {
			inputError(bind, "regForm", "password");
			return "register";
		}
		
		return "redirect:/login-form";
	}
}
