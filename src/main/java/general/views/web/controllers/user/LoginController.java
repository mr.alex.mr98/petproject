package general.views.web.controllers.user;

import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import general.service.requests.*;
import general.views.*;


@Controller
public class LoginController extends AbstractController {
	
	public LoginController(UserService userService) {
		super(userService);
	}
	
	@GetMapping("/personal-area")
	public String index(Model model) {
		model.addAttribute("name", getCurrentUser().getName());
		
		return "personal-area";
	}
	
	@GetMapping("/login-form")
	public String getAuth() {
		return "login-form";
	}
}
