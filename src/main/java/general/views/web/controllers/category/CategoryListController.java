package general.views.web.controllers.category;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import general.service.requests.*;
import general.views.*;


@Controller
public class CategoryListController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public CategoryListController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/categories")
	public String getCategories(Model model) {
		model.addAttribute("list", categoryService.findAll(getCurrentUser().getId()));
		
		return "/categories/categories_list";
	}
}
