package general.views.web.controllers.category;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class ChangeCategoryController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public ChangeCategoryController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/change-category")
	public String getChangeCat(Model model) {
		model.addAttribute("list", categoryService.findAll(getCurrentUser().getId()));
		model.addAttribute("changeNameForm", new ChangeNameByIdForm());
		
		return "/categories/change_cat_name";
	}
	
	@PostMapping("/change-category")
	public String postChangeCat(@ModelAttribute("changeNameForm") @Valid ChangeNameByIdForm form,
								BindingResult bind,
								Model model) {
		if (!categoryService.change(form.getId(), form.getNewName(), getCurrentUser().getId())) {
			model.addAttribute("list", categoryService.findAll(getCurrentUser().getId()));
			inputError(bind, "changeNameForm", "newName");
			return "/categories/change_cat_name";
		}
		
		return "redirect:/categories";
	}
}
