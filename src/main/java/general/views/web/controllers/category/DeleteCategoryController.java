package general.views.web.controllers.category;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class DeleteCategoryController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public DeleteCategoryController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/delete-category")
	public String getDeleteCat(Model model) {
		model.addAttribute("list", categoryService.findAll(getCurrentUser().getId()));
		model.addAttribute("deleteByIdForm", new RequestByIdForm());
		
		return "/categories/delete_cat";
	}
	
	@PostMapping("/delete-category")
	public String postDeleteCat(@ModelAttribute("deleteByIdForm") @Valid RequestByIdForm form,
								BindingResult bind,
								Model model) {
		if (!categoryService.delete(form.getId(), getCurrentUser().getId())) {
			model.addAttribute("list", categoryService.findAll(getCurrentUser().getId()));
			inputError(bind, "deleteByIdForm", "id");
			return "/categories/delete_cat";
		}
		
		return "redirect:/categories";
	}
}
