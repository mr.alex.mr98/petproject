package general.views.web.controllers.category;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class CreateCategoryController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public CreateCategoryController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/create-category")
	public String getCreateCat(Model model) {
		model.addAttribute("createByNameForm", new CreateByNameForm());
		
		return "/categories/create_cat";
	}
	
	@PostMapping("/create-category")
	public String postCreateCat(@ModelAttribute("createByNameForm") @Valid CreateByNameForm form,
								BindingResult bind) {
		if (!categoryService.create(form.getName(), getCurrentUser().getId())
							.isPresent()) {
			inputError(bind, "createByNameForm", "name");
			return "/categories/create_cat";
		}
		
		return "redirect:/categories";
	}
}
