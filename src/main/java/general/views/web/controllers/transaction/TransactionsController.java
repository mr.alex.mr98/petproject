package general.views.web.controllers.transaction;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.transactions.*;


@Controller
public class TransactionsController extends AbstractController {
	
	private final TransactionService transactionService;
	private final AccountService accountService;
	private final CategoryService categoryService;
	
	@Autowired
	public TransactionsController(UserService userService,
								  TransactionService transactionService,
								  AccountService accountService,
								  CategoryService categoryService) {
		super(userService);
		this.transactionService = transactionService;
		this.accountService = accountService;
		this.categoryService = categoryService;
	}
	
	@GetMapping("/transaction")
	public String getTransaction(Model model) {
		model.addAttribute("transactionForm", new TransactionForm());
		model.addAttribute("accList", accountService.findAll(getCurrentUser().getId()));
		model.addAttribute("catList", categoryService.findAll(getCurrentUser().getId()));
		
		return "/transaction/transaction";
	}
	
	@PostMapping("/transaction")
	public String postTransaction(@ModelAttribute("transactionForm") @Valid TransactionForm form,
								  BindingResult bind,
								  Model model) {
		if (!transactionService.make(form.getFromAccountId(),
									 form.getToAccountId(),
									 form.getValue(),
									 form.getCategoryId(),
									 getCurrentUser().getId())
							   .isPresent()) {
			model.addAttribute("accList", accountService.findAll(getCurrentUser().getId()));
			model.addAttribute("catList", categoryService.findAll(getCurrentUser().getId()));
			inputError(bind, "transactionForm", "value");
			
			return "/transaction/transaction";
		}
		
		return "redirect:/accounts";
	}
}
