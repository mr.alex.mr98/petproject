package general.views.web.controllers.accounts;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class CreateAccountController extends AbstractController {
	
	private final AccountService accountService;
	
	@Autowired
	public CreateAccountController(UserService userService, AccountService accountService) {
		super(userService);
		this.accountService = accountService;
	}
	
	@GetMapping("/create-account")
	public String getChangeAcc(Model model) {
		model.addAttribute("createByNameForm", new CreateByNameForm());
		
		return "/accounts/create_acc";
	}
	
	@PostMapping("/create-account")
	public String postChangeAcc(@ModelAttribute("createByNameForm") @Valid CreateByNameForm form,
								BindingResult bind) {
		if (!accountService.create(form.getName(), getCurrentUser().getId())
						   .isPresent()) {
			inputError(bind, "createByNameForm", "name");
			return "/accounts/create_acc";
		}
		
		return "redirect:/accounts";
	}
}
