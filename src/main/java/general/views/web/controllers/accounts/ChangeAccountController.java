package general.views.web.controllers.accounts;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class ChangeAccountController extends AbstractController {
	
	private final AccountService accountService;
	
	@Autowired
	public ChangeAccountController(UserService userService, AccountService accountService) {
		super(userService);
		this.accountService = accountService;
	}
	
	@GetMapping("/change-account")
	public String getChangeAcc(Model model) {
		model.addAttribute("list", accountService.findAll(getCurrentUser().getId()));
		model.addAttribute("changeNameForm", new ChangeNameByIdForm());
		
		return "/accounts/change_acc_name";
	}
	
	@PostMapping("/change-account")
	public String postChangeAcc(@ModelAttribute("changeNameForm") @Valid ChangeNameByIdForm form,
								BindingResult bind,
								Model model) {
		if (!accountService.change(form.getId(), form.getNewName(), getCurrentUser().getId())) {
			model.addAttribute("list", accountService.findAll(getCurrentUser().getId()));
			inputError(bind, "changeNameForm", "newName");
			return "/accounts/change_acc_name";
		}
		
		return "redirect:/accounts";
	}
}
