package general.views.web.controllers.accounts;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import general.service.requests.*;
import general.views.*;


@Controller
public class AccountsListController extends AbstractController {
	
	private final AccountService accountService;
	
	@Autowired
	public AccountsListController(UserService userService, AccountService accountService) {
		super(userService);
		this.accountService = accountService;
	}
	
	@GetMapping("/accounts")
	public String getAccounts(Model model) {
		model.addAttribute("list", accountService.findAll(getCurrentUser().getId()));
		
		return "/accounts/accounts_list";
	}
}
