package general.views.web.controllers.accounts;

import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.*;


@Controller
public class DeleteAccountController extends AbstractController {
	
	private final AccountService accountService;
	
	@Autowired
	public DeleteAccountController(UserService userService, AccountService accountService) {
		super(userService);
		this.accountService = accountService;
	}
	
	@GetMapping("/delete-account")
	public String getDeleteAcc(Model model) {
		model.addAttribute("list", accountService.findAll(getCurrentUser().getId()));
		model.addAttribute("deleteByIdForm", new RequestByIdForm());
		
		return "/accounts/delete_acc";
	}
	
	@PostMapping("/delete-account")
	public String postDeleteAcc(@ModelAttribute("deleteByIdForm") @Valid RequestByIdForm form,
								BindingResult bind,
								Model model) {
		if (!accountService.delete(form.getId(), getCurrentUser().getId())) {
			model.addAttribute("list", accountService.findAll(getCurrentUser().getId()));
			inputError(bind, "deleteByIdForm", "id");
			return "/accounts/delete_acc";
		}
		
		return "redirect:/accounts";
	}
}
