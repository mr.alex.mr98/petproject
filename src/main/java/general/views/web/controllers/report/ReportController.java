package general.views.web.controllers.report;

import static general.views.utils.InputConverter.*;
import static general.views.utils.InputValidator.*;
import static general.views.utils.WebErrors.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.web.forms.report.*;


@Controller
public class ReportController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public ReportController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/report")
	public String getReport(Model model) {
		model.addAttribute("reportForm", new ReportForm());
		model.addAttribute("list", new ArrayList<>(0));
		
		return "/report/report";
	}
	
	@PostMapping("/report")
	public String postReport(@ModelAttribute("reportForm") @Valid ReportForm form,
							 BindingResult bind,
							 Model model) {
		if (!isValidDateFormat(form.getFrom()) || !isValidDateFormat(form.getTill())) {
			model.addAttribute("list", new ArrayList<>(0));
			inputError(bind, "reportForm", "till");
			
			return "/report/report";
		}
		
		model.addAttribute("list", categoryService.report(strToTime(form.getFrom()),
														  strToTime(form.getTill()),
														  getCurrentUser().getId()));
		
		return "/report/report";
	}
}