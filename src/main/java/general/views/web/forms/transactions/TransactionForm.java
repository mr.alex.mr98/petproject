package general.views.web.forms.transactions;

import java.math.*;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class TransactionForm {
	
	@NotEmpty
	private int fromAccountId;
	
	@NotEmpty
	private int toAccountId;
	
	@NotEmpty
	private BigDecimal value;
	
	@NotEmpty
	private int categoryId;
}
