package general.views.web.forms.report;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class ReportForm {
	
	@NotEmpty
	private String from;
	
	@NotEmpty
	private String till;
}
