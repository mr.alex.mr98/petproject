package general.views.web.forms.user;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class AuthForm {
	
	@Email
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String password;
}
