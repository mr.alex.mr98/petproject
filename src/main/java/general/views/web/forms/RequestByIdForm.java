package general.views.web.forms;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class RequestByIdForm {
	
	@NotEmpty
	private int id;
}
