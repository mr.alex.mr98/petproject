package general.views.web.forms;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class ChangeNameByIdForm {
	
	@NotEmpty
	private int id;
	
	@NotEmpty
	private String newName;
}
