package general.views.web.forms;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class CreateByNameForm {
	
	@NotEmpty
	private String name;
}
