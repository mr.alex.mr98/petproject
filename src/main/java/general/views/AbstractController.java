package general.views;

import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.context.*;
import org.springframework.stereotype.*;

import general.security.*;
import general.service.entity.*;
import general.service.requests.*;
import lombok.*;


@Service
@RequiredArgsConstructor
public abstract class AbstractController {
	
	@Autowired
	private final UserService userService;
	
	protected UserDTO getCurrentUser() {
		CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext()
																				 .getAuthentication()
																				 .getPrincipal();
		
		return userService.findById(userDetails.getId());
	}
}
