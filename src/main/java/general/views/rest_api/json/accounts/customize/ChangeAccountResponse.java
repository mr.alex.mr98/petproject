package general.views.rest_api.json.accounts.customize;

import lombok.*;


@AllArgsConstructor
@Data
public class ChangeAccountResponse {
	
	private String message;
}
