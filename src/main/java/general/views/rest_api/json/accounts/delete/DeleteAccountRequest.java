package general.views.rest_api.json.accounts.delete;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class DeleteAccountRequest {
	
	@NotNull
	private int accountId;
}
