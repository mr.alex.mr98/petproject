package general.views.rest_api.json.accounts.delete;

import lombok.*;


@AllArgsConstructor
@Data
public class DeleteAccountResponse {
	
	private String message;
}
