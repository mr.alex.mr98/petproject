package general.views.rest_api.json.accounts.create;

import lombok.*;


@Data
@AllArgsConstructor
public class CreateAccountResponse {
	
	private String message;
}
