package general.views.rest_api.json.accounts.customize;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class ChangeAccountRequest {
	
	@NotNull
	private int accountId;
	
	@NotNull
	private String newName;
}
