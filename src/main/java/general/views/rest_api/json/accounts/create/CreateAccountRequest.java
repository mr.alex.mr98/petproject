package general.views.rest_api.json.accounts.create;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class CreateAccountRequest {
	
	@NotNull
	private String name;
}
