package general.views.rest_api.json.user.auth;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class AuthRequest {
	
	@Email
	@NotNull
	private String email;
	
	@NotNull
	private String password;
}
