package general.views.rest_api.json.user.registration;

import lombok.*;


@Data
public class RegistrationRequest {
	
	private String email = null;
	private String password = null;
	private String name = null;
}
