package general.views.rest_api.json.user.registration;

import lombok.*;


@AllArgsConstructor
@Data
public class RegistrationResponse {
	
	private String done;
}
