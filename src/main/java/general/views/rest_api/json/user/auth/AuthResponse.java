package general.views.rest_api.json.user.auth;

import lombok.*;


@Data
@AllArgsConstructor
public class AuthResponse {
	
	private int userId;
	private String name;
	private String email;
}
