package general.views.rest_api.json.report;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class ReportRequest {
	
	@NotNull
	private String from;
	
	@NotNull
	private String till;
}
