package general.views.rest_api.json.categories.customize;

import lombok.*;


@AllArgsConstructor
@Data
public class ChangeCategoryResponse {
	
	private String message;
}
