package general.views.rest_api.json.categories.delete;

import lombok.*;


@AllArgsConstructor
@Data
public class DeleteCategoryResponse {
	
	private String message;
}
