package general.views.rest_api.json.categories.create;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class CreateCategoryRequest {
	
	@NotNull
	private String name;
}
