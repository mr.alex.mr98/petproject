package general.views.rest_api.json.categories.delete;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class DeleteCategoryRequest {
	
	@NotNull
	private int categoryId;
}
