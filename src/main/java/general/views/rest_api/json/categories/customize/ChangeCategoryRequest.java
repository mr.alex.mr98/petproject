package general.views.rest_api.json.categories.customize;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class ChangeCategoryRequest {
	
	@NotNull
	private int categoryId;
	
	@NotNull
	private String newName;
}
