package general.views.rest_api.json.categories.create;

import lombok.*;


@Data
@AllArgsConstructor
public class CreateCategoryResponse {
	
	private String message;
}
