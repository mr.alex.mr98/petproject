package general.views.rest_api.json.transactions;

import lombok.*;


@Data
@AllArgsConstructor
public class TransactionResponse {
	
	private String message;
}
