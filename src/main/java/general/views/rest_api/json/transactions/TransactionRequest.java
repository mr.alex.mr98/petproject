package general.views.rest_api.json.transactions;

import java.math.*;

import javax.validation.constraints.*;

import lombok.*;


@Data
public class TransactionRequest {
	
	@NotNull
	private int fromAccountId;
	
	@NotNull
	private int toAccountId;
	
	@NotNull
	private BigDecimal value;
	
	@NotNull
	private int categoryId;
}
