package general.views.rest_api.message_response;

import general.views.rest_api.json.accounts.create.*;
import general.views.rest_api.json.accounts.customize.*;
import general.views.rest_api.json.accounts.delete.*;


public class AccountResponses {
	
	public static DeleteAccountResponse deleteAccResponse() {
		return new DeleteAccountResponse("You have deleted the account!");
	}
	
	public static ChangeAccountResponse changeAccResponse() {
		return new ChangeAccountResponse("You have changed the account!");
	}
	
	public static CreateAccountResponse createAccResponse() {
		return new CreateAccountResponse("You have created the account!");
	}
}
