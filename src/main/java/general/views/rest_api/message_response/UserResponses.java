package general.views.rest_api.message_response;

import general.views.rest_api.json.user.registration.*;


public class UserResponses {
	
	public static RegistrationResponse registerResponse() {
		return new RegistrationResponse("Registration has been done!");
	}
}
