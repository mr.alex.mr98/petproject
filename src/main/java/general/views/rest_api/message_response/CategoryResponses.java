package general.views.rest_api.message_response;

import general.views.rest_api.json.categories.create.*;
import general.views.rest_api.json.categories.customize.*;
import general.views.rest_api.json.categories.delete.*;


public class CategoryResponses {
	
	public static DeleteCategoryResponse deleteCatResponse() {
		return new DeleteCategoryResponse("You have deleted the category!");
	}
	
	public static ChangeCategoryResponse changeCatResponse() {
		return new ChangeCategoryResponse("You have changed the category!");
	}
	
	public static CreateCategoryResponse createCatResponse() {
		return new CreateCategoryResponse("You have created the category!");
	}
}
