package general.views.rest_api.message_response;

import general.views.rest_api.json.transactions.*;


public class TransactionResponses {
	
	public static TransactionResponse transactionResponse() {
		return new TransactionResponse("You have made the transaction!");
	}
}
