package general.views.rest_api.controller;

import static org.springframework.http.ResponseEntity.*;
import static general.views.rest_api.message_response.CategoryResponses.*;
import static general.views.utils.InputConverter.*;
import static general.views.utils.InputValidator.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.*;

import general.service.entity.*;
import general.service.requests.*;
import general.views.*;
import general.views.rest_api.json.categories.create.*;
import general.views.rest_api.json.categories.customize.*;
import general.views.rest_api.json.categories.delete.*;
import general.views.rest_api.json.report.*;


@RestController
@RequestMapping("/api")
public class CategoryController extends AbstractController {
	
	private final CategoryService categoryService;
	
	@Autowired
	public CategoryController(UserService userService, CategoryService categoryService) {
		super(userService);
		this.categoryService = categoryService;
	}
	
	@GetMapping("/categories")
	public ResponseEntity<List<CategoryDTO>> userCategories() {
		return ok(categoryService.findAll(getCurrentUser().getId()));
	}
	
	@PostMapping("/delete-category")
	public ResponseEntity<DeleteCategoryResponse> deleteCategory(@RequestBody @Valid DeleteCategoryRequest deleteRequest) {
		if (!categoryService.delete(deleteRequest.getCategoryId(), getCurrentUser().getId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(deleteCatResponse());
	}
	
	@PostMapping("/change-category")
	public ResponseEntity<ChangeCategoryResponse> changeCategory(@RequestBody @Valid ChangeCategoryRequest changeRequest) {
		if (!categoryService.change(changeRequest.getCategoryId(),
									changeRequest.getNewName(),
									getCurrentUser().getId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(changeCatResponse());
	}
	
	@PostMapping("/create-category")
	public ResponseEntity<CreateCategoryResponse> createCategory(@RequestBody @Valid CreateCategoryRequest createRequest) {
		if (!categoryService.create(createRequest.getName(), getCurrentUser().getId())
							.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(createCatResponse());
	}
	
	@PostMapping("/report")
	public ResponseEntity<List<ReportDTO>> categoriesReport(@RequestBody @Valid ReportRequest reportRequest) {
		if (!isValidDateFormat(reportRequest.getFrom()) || !isValidDateFormat(reportRequest.getTill())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(categoryService.report(strToTime(reportRequest.getFrom() + " 00:00:01"),
										 strToTime(reportRequest.getTill() + " 23:59:59"),
										 getCurrentUser().getId()));
	}
}
