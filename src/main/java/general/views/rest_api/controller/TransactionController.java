package general.views.rest_api.controller;

import static org.springframework.http.ResponseEntity.*;
import static general.views.rest_api.message_response.TransactionResponses.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.requests.*;
import general.views.*;
import general.views.rest_api.json.transactions.*;


@RestController
@RequestMapping("/api")
public class TransactionController extends AbstractController {
	
	private final TransactionService transactionService;
	
	@Autowired
	public TransactionController(UserService userService, TransactionService transactionService) {
		super(userService);
		this.transactionService = transactionService;
	}
	
	@PostMapping("/transaction")
	public ResponseEntity<TransactionResponse> makeTransaction(@RequestBody @Valid TransactionRequest transRequest) {
		if (!transactionService.make(transRequest.getFromAccountId(),
									 transRequest.getToAccountId(),
									 transRequest.getValue(),
									 transRequest.getCategoryId(),
									 getCurrentUser().getId())
							   .isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(transactionResponse());
	}
}
