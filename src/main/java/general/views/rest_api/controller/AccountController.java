package general.views.rest_api.controller;

import static org.springframework.http.ResponseEntity.*;
import static general.views.rest_api.message_response.AccountResponses.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.*;

import general.service.entity.*;
import general.service.requests.*;
import general.views.*;
import general.views.rest_api.json.accounts.create.*;
import general.views.rest_api.json.accounts.customize.*;
import general.views.rest_api.json.accounts.delete.*;


@RestController
@RequestMapping("/api")
public class AccountController extends AbstractController {
	
	private final AccountService accountService;
	
	@Autowired
	public AccountController(UserService userService, AccountService accountService) {
		super(userService);
		this.accountService = accountService;
	}
	
	@GetMapping("/accounts")
	public ResponseEntity<List<AccountDTO>> userAccounts() {
		
		return ok(accountService.findAll(getCurrentUser().getId()));
	}
	
	@PostMapping("/delete-account")
	public ResponseEntity<DeleteAccountResponse> deleteAccount(@RequestBody @Valid DeleteAccountRequest deleteRequest) {
		if (!accountService.delete(deleteRequest.getAccountId(), getCurrentUser().getId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(deleteAccResponse());
	}
	
	@PostMapping("/change-account")
	public ResponseEntity<ChangeAccountResponse> changeAccount(@RequestBody @Valid ChangeAccountRequest changeRequest) {
		if (!accountService.change(changeRequest.getAccountId(),
								   changeRequest.getNewName(),
								   getCurrentUser().getId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(changeAccResponse());
	}
	
	@PostMapping("/create-account")
	public ResponseEntity<CreateAccountResponse> createAccount(@RequestBody @Valid CreateAccountRequest createRequest) {
		if (!accountService.create(createRequest.getName(), getCurrentUser().getId())
						   .isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(createAccResponse());
	}
}
