package general.views.rest_api.controller;

import static org.springframework.http.ResponseEntity.*;
import static general.views.rest_api.message_response.UserResponses.*;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import general.service.entity.*;
import general.service.requests.*;
import general.views.*;
import general.views.rest_api.json.user.registration.*;


@RestController
@RequestMapping("/api")
public class UserController extends AbstractController {
	
	public UserController(UserService userService) {
		super(userService);
	}
	
	@GetMapping("/user-info")
	public ResponseEntity<UserDTO> getUserInfo() {
		return ok(getCurrentUser());
	}
	
	@PostMapping("/register")
	public ResponseEntity<RegistrationResponse> registration(@RequestBody @Valid RegistrationRequest regRequest) {
		if (!userService.register(regRequest.getEmail(),
								  regRequest.getPassword(),
								  regRequest.getName())
						.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								 .build();
		}
		
		return ok(registerResponse());
	}
}
