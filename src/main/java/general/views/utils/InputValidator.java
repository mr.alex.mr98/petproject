package general.views.utils;

import org.springframework.stereotype.*;

import lombok.*;


@RequiredArgsConstructor
@Service
public class InputValidator {
	
	public static boolean isValidDateFormat(String time) {
		return time != null && time.matches("\\d{4}-\\d{2}-\\d{2}");
	}
}
