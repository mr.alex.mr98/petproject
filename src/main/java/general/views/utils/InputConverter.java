package general.views.utils;

import org.springframework.stereotype.*;

import java.sql.*;

import general.service.exception.*;


@Service
public class InputConverter {
	
	public static Timestamp strToTime(String time) {
		try {
			return Timestamp.valueOf(time);
		} catch (Exception e) {
			throw new CustomException(e);
		}
	}
}
