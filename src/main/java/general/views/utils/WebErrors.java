package general.views.utils;

import org.springframework.stereotype.*;
import org.springframework.validation.*;


@Service
public class WebErrors {
	
	public static void authError(BindingResult result) {
		result.addError(new FieldError("authForm",
									   "password", "Wrong e-mail or password!"));
	}
	
	public static void inputError(BindingResult result, String formName, String field) {
		result.addError(new FieldError(formName,
									   field,
									   "Invalid data!"));
	}
}
