package general.views.console_view;

import org.springframework.stereotype.*;

import java.util.*;
import java.util.stream.*;

import general.service.entity.*;
import general.service.requests.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class AccountsManager {
	
	private final Scanner scanner = new Scanner(System.in);
	private final AccountService accountService;
	private final TransactionsManager transactionsManager;
	private int userId;
	private List<AccountDTO> accounts;
	
	public void userAccountsManagement(int userId) {
		this.userId = userId;
		accounts = accountService.findAll(userId);
		
		do {
			System.out.println("\n1 - Show your accounts\n" +
							   "2 - Send money\n" +
							   "3 - Create an account\n" +
							   "4 - Change account name\n" +
							   "5 - Delete an account\n" +
							   "0 - Back");
		} while (!userAccountsManagementChoice(scanner("Choice: ")).equals("0"));
	}
	
	public String userAccountsManagementChoice(String i) {
		if (i.equals("1")) {
			showAccounts();
		} else if (i.equals("2")) {
			sendMoney();
		} else if (i.equals("3")) {
			createAccount();
		} else if (i.equals("4")) {
			customiseAccount();
		} else if (i.equals("5")) {
			deleteAccount();
		} else if (i.equals("0")) {
			return i;
		} else {
			System.err.println("Press correct button please\n");
		}
		return i;
	}
	
	private String scanner(String message) {
		System.out.print(message);
		return scanner.next();
	}
	
	private void showAccounts() {
		System.out.println();
		
		if (accounts.isEmpty()) {
			System.out.println("\nYou have no accounts!");
		} else {
			IntStream.range(0, accounts.size())
					 .forEach(i -> System.out.println((i + 1) + " -  " + accounts.get(i)
																				 .getName() + " : " + accounts.get(i)
																											  .getBalance()));
		}
	}
	
	private void sendMoney() {
		transactionsManager.trySendMoney(userId);
	}
	
	private void createAccount() {
		String accountName = scanner("\nEnter an account name you wanna create: ");
		System.out.println(accountService.create(accountName, userId)
										 .map(a -> "\nYou have created the account")
										 .orElse("\nYou can't create such account!"));
	}
	
	private void customiseAccount() {
		if (accounts.isEmpty()) {
			showAccounts();
		} else {
			int accountIdToChange = requestAccountId(scanner("Enter an account number you wanna change: "));
			String newName = scanner("Enter a new name: ");
			
			if (!accountService.change(accountIdToChange, newName, userId)) {
				System.out.println("\nYou don't have the account you wanna change!");
			} else {
				System.out.println("\nYou have changed the account name");
			}
		}
	}
	
	private void deleteAccount() {
		if (accounts.isEmpty()) {
			showAccounts();
		} else {
			int accountIdToDelete = requestAccountId(scanner("Enter an account number you wanna delete: "));
			if (!accountService.delete(accountIdToDelete, userId)) {
				System.out.println("\nYou can't delete this account!");
			} else {
				System.out.println("\nYou have deleted the account");
			}
		}
	}
	
	private int requestAccountId(String request) {
		return accountService.findAll(userId)
							 .get(Integer.parseInt(request) - 1)
							 .getId();
	}
}

