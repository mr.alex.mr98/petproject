package general.views.console_view;

import org.springframework.boot.*;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.*;

import java.util.*;

import general.dao.repository.*;
import general.service.entity.*;
import general.service.requests.*;
import lombok.*;


@Component
@RequiredArgsConstructor
@Profile("!test")
public class ConsoleView implements CommandLineRunner {
	
	private final Scanner scanner = new Scanner(System.in);
	private final UserService userService;
	private final UserRepository userRepository;
	private final UserManager userManager;
	
	@Override
	public void run(String... args) throws Exception {
		startProgram();
	}
	
	public void startProgram() {
		System.out.println(userRepository.findByEmail("test@test.com")
										 .get()
										 .getPassword());
		
		do {
			System.out.println("\n1 - Log in\n" + "2 - Register\n" + "0 - Exit");
		} while (!startChoice(scanner("Choice: ")).equals("0"));
	}
	
	private String startChoice(String i) {
		if (i.equals("1")) {
			information();
		} else if (i.equals("2")) {
			registration();
		} else if (i.equals("0")) {
			System.exit(-1);
		} else {
			System.err.println("Press correct button please\n");
		}
		return i;
	}
	
	private String scanner(String message) {
		System.out.print(message);
		return scanner.next();
	}
	
	private void information() {
		Optional<UserDTO> user = login();
		if (user.isPresent()) {
			userManager.userInformation(user.get());
		} else {
			System.err.println("\nSuch user doesnt exist!");
			System.exit(-1);
		}
	}
	
	private void registration() {
		System.out.println(userService.register(scanner("\nEnter e-mail: "),
												scanner("Enter password: "),
												scanner("Enter name: "))
									  .map(r -> "\nRegistration has been done!")
									  .orElse("\nError!"));
	}
	
	private Optional<UserDTO> login() {
		return userService.findByEmailAndPassword(scanner("\nEnter e-mail: "), scanner("Enter password: "));
	}
}
