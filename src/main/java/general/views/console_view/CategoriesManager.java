package general.views.console_view;

import static general.views.utils.InputConverter.*;
import static general.views.utils.InputValidator.*;

import org.springframework.stereotype.*;

import java.util.*;
import java.util.stream.*;

import general.service.entity.*;
import general.service.requests.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class CategoriesManager {
	
	private final Scanner scanner = new Scanner(System.in);
	private final CategoryService categoryService;
	private int userId;
	private List<CategoryDTO> categories;
	
	void userCategoriesManagement(int userId) {
		this.userId = userId;
		categories = categoryService.findAll(userId);
		
		do {
			System.out.println("\n1 - Show your categories\n" +
							   "2 - Create a new category\n" +
							   "3 - Delete a category\n" +
							   "4 - Change a category\n" +
							   "5 - Show all categories spending on period\n" +
							   "0 - Back");
		} while (!userCategoriesManagementChoice(scanner("Choice: ")).equals("0"));
	}
	
	private String userCategoriesManagementChoice(String i) {
		if (i.equals("1")) {
			showCategories();
		} else if (i.equals("2")) {
			create();
		} else if (i.equals("3")) {
			delete();
		} else if (i.equals("4")) {
			customiseCategory();
		} else if (i.equals("5")) {
			showCategoriesSpending();
		} else if (i.equals("0")) {
			return i;
		} else {
			System.err.println("Press correct button please\n");
		}
		return i;
	}
	
	private String scanner(String message) {
		System.out.print(message);
		return scanner.next();
	}
	
	private void showCategories() {
		System.out.println();
		
		if (categories.isEmpty()) {
			System.out.println("\nYou have no categories!");
		} else {
			IntStream.range(0, categories.size())
					 .forEach(i -> {
						 System.out.println((i + 1) + " -  " + categories.get(i)
																		 .getName());
					 });
		}
	}
	
	private void create() {
		System.out.println("\n" + categoryService.create(scanner("\nEnter a category name you wanna create: "), userId)
												 .map(c -> "You have created the category")
												 .orElse("You already have such category!"));
	}
	
	private void delete() {
		if (categories.isEmpty()) {
			showCategories();
		} else {
			int categoryIdToDelete = requestCategoryId(scanner("Enter a category number you wanna delete: "));
			
			if (categoryService.delete(categoryIdToDelete, userId)) {
				System.out.println("\nYou have deleted the category");
			} else {
				System.out.println("\nYou can't delete this category!");
			}
		}
	}
	
	private void customiseCategory() {
		if (categories.isEmpty()) {
			showCategories();
		} else {
			int categoryIdToChange = requestCategoryId(scanner("Enter a category number you wanna change: "));
			String newName = scanner("Enter a new name: ");
			
			if (!categoryService.change(categoryIdToChange, newName, userId)) {
				System.out.println("\nYou have changed the category name");
			} else {
				System.out.println("\nYou don't have the category you wanna change!");
			}
		}
	}
	
	private void showCategoriesSpending() {
		if (categories.isEmpty()) {
			showCategories();
		} else {
			System.out.println("\nEnter dates in format «YYYY-MM-DD»:");
			
			String from = scanner("from: ") + " 00:00:01";
			String till = scanner("till: ") + " 23:59:59";
			
			if (!isValidDateFormat(from) || !isValidDateFormat(till)) {
				System.out.println("/-/-/-/-/-/");
			} else {
				System.out.println();
				categoryService.report(strToTime(from),
									   strToTime(till),
									   userId)
							   .forEach(e -> {
								   System.out.println(e.getCatName() + " - " + e.getValue());
							   });
			}
		}
	}
	
	private int requestCategoryId(String request) {
		return categoryService.findAll(userId)
							  .get(Integer.parseInt(request) - 1)
							  .getId();
	}
}
