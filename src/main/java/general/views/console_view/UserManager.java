package general.views.console_view;

import org.springframework.stereotype.*;

import java.util.*;

import general.service.entity.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class UserManager {
	
	private final Scanner scanner = new Scanner(System.in);
	private final AccountsManager accountsManager;
	private final CategoriesManager categoriesManager;
	private int userId;
	
	public void userInformation(UserDTO user) {
		userId = user.getId();
		System.out.println("\nHello, " + user.getName() + "!");
		
		do {
			System.out.println("\n1 - Accounts management\n" + "2 - Categories management\n" + "0 - Exit");
		} while (!userInformationChoice(scanner("Choice: ")).equals("0"));
	}
	
	private String userInformationChoice(String i) {
		if (i.equals("1")) {
			accountsManager.userAccountsManagement(userId);
		} else if (i.equals("2")) {
			categoriesManager.userCategoriesManagement(userId);
		} else if (i.equals("0")) {
			System.out.println("\nYou have logged out.");
			System.exit(-1);
		} else {
			System.err.println("Press correct button please\n");
		}
		return i;
	}
	
	private String scanner(String message) {
		System.out.print(message);
		return scanner.next();
	}
}
