package general.views.console_view;

import org.springframework.stereotype.*;

import java.math.*;
import java.util.*;
import java.util.stream.*;

import general.service.entity.*;
import general.service.requests.*;
import lombok.*;


@RequiredArgsConstructor
@Component
public class TransactionsManager {
	
	private final Scanner scanner = new Scanner(System.in);
	private final CategoryService categoryService;
	private final AccountService accountService;
	private final TransactionService transactionService;
	private int userId;
	private List<CategoryDTO> categories;
	private List<AccountDTO> accounts;
	
	public void trySendMoney(int userId) {
		categories = categoryService.findAll(userId);
		accounts = accountService.findAll(userId);
		this.userId = userId;
		
		if (accounts.isEmpty()) {
			System.out.println("\nYou have no accounts!");
		} else {
			accountChoosing();
			
			if (transactionService.make(requestAccountId(scanner("from: ")),
										requestAccountId(scanner("to: ")),
										requestValue(scanner("money value: ")),
										requestCategoryId(),
										userId)
								  .isPresent()) {
				System.out.println("\nYou have sent the money");
			} else {
				System.out.println("\nThis account doesn't have enough money!");
			}
		}
	}
	
	private void accountChoosing() {
		System.out.println();
		IntStream.range(0, accounts.size())
				 .forEach(i -> {
					 System.out.println((i + 1) + " -  " + accounts.get(i)
																   .getName() + " - " + accounts.get(i)
																								.getBalance());
				 });
	}
	
	private int requestAccountId(String request) {
		return accounts.get(Integer.parseInt(request) - 1)
					   .getId();
	}
	
	private String scanner(String message) {
		System.out.print(message);
		return scanner.next();
	}
	
	private BigDecimal requestValue(String value) {
		return BigDecimal.valueOf(Long.parseLong(value));
	}
	
	private int requestCategoryId() {
		
		if (categories.isEmpty()) {
			categories.add(categoryService.create("Others", userId)
										  .get());
			return categories.get(0)
							 .getId();
		}
		
		System.out.println();
		IntStream.range(0, categories.size())
				 .forEach(i -> {
					 System.out.println((i + 1) + " -  " + categories.get(i)
																	 .getName());
				 });
		System.out.println("\nChoose transaction category: ");
		return categories.get(Integer.parseInt(scanner("Choice: ")) - 1)
						 .getId();
	}
}
