package general.security;

import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.*;

import java.util.*;

import lombok.*;


@Getter
@Setter
@RequiredArgsConstructor
public class CustomUserDetails implements UserDetails {
	
	private final int id;
	private final String email;
	private final String password;
	private final Collection<CustomGrantedAuthority> getAuthorities;
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return getAuthorities;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public String getUsername() {
		return email;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}
}
