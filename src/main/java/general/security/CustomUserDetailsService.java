package general.security;

import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;

import java.util.stream.*;

import general.dao.entity.*;
import general.dao.repository.*;
import lombok.*;


@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
	
	private final UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity user = userRepository.findByEmail(email)
										.get();
		
		if (user == null) {
			throw new UsernameNotFoundException("Can't find such user!");
		}
		
		return new CustomUserDetails(user.getId(),
									 user.getEmail(),
									 user.getPassword(),
									 user.getRoles()
										 .stream()
										 .map(CustomGrantedAuthority::new)
										 .collect(Collectors.toList()));
	}
}
