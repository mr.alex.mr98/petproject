package general.security;

import static general.security.UserRole.*;

import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.security.crypto.password.*;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
			.disable()
			.authorizeRequests()
			.antMatchers("/login-form")
			.permitAll()
			.antMatchers("/register")
			.permitAll()
			.antMatchers("/api/register")
			.permitAll()
			.anyRequest()
			.hasAnyRole(USER.name(), ADMIN.name())
			.and()
			.formLogin()
			.usernameParameter("email")
			.passwordParameter("password")
			.loginPage("/login-form")
			.loginProcessingUrl("/login")
			.defaultSuccessUrl("/personal-area")
			.and()
			.logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/login-form");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}
}
