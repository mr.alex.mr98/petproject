package general.security;

import org.springframework.security.core.*;

import lombok.*;


@RequiredArgsConstructor
public class CustomGrantedAuthority implements GrantedAuthority {
	
	private final UserRole userRole;
	private final String role = "ROLE_";
	
	@Override
	public String getAuthority() {
		return role + userRole.name();
	}
}
